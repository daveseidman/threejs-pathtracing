let SCREEN_WIDTH;
let SCREEN_HEIGHT;
let canvas, context;
let container, stats;
let controls;
let pathTracingScene, screenCopyScene, screenOutputScene;
let pathTracingUniforms, screenCopyUniforms, screenOutputUniforms;
let pathTracingDefines;
let pathTracingVertexShader, pathTracingFragmentShader;
let screenCopyVertexShader, screenCopyFragmentShader;
let screenOutputVertexShader, screenOutputFragmentShader;
let pathTracingGeometry, pathTracingMaterial, pathTracingMesh;
let screenCopyGeometry, screenCopyMaterial, screenCopyMesh;
let screenOutputGeometry, screenOutputMaterial, screenOutputMesh;
let pathTracingRenderTarget, screenCopyRenderTarget;
let quadCamera, worldCamera;
let renderer, clock;
let frameTime, elapsedTime;
let fovScale;
let apertureSize = 0.0;
let focusDistance = 132.0;
let pixelRatio = 1.0;
let windowIsBeingResized = false;
let sampleCounter = 1.0;
let frameCounter = 1.0;
let cameraIsMoving = false;
let cameraRecentlyMoving = false;
let isPaused = true;
let oldYawRotation, oldPitchRotation;
let sunAngularDiameterCos;
let EPS_intersect;
let blueNoiseTexture;
let cameraPosition = new THREE.Vector3();
let cameraPositionPrevious = new THREE.Vector3();

// the following variables will be used to calculate rotations and directions from the camera
let cameraDirectionVector = new THREE.Vector3(); //for moving where the camera is looking
let cameraRightVector = new THREE.Vector3(); //for strafing the camera right and left
let cameraUpVector = new THREE.Vector3(); //for moving camera up and down
let cameraWorldQuaternion = new THREE.Quaternion(); //for rotating scene objects to match camera's current rotation
let cameraControlsObject; //for positioning and moving the camera itself
let cameraControlsYawObject; //allows access to control camera's left/right movements through mobile input
let cameraControlsPitchObject; //allows access to control camera's up/down movements through mobile input

let mouseControl = true;
// let pointerlockChange;
let fileLoader = new THREE.FileLoader();


function onWindowResize(event) {
	windowIsBeingResized = true;
	SCREEN_WIDTH = document.body.clientWidth; //window.innerWidth;
	SCREEN_HEIGHT = document.body.clientHeight; //window.innerHeight;
	renderer.setPixelRatio(pixelRatio);
	renderer.setSize(SCREEN_WIDTH, SCREEN_HEIGHT);
	pathTracingUniforms.uResolution.value.x = context.drawingBufferWidth;
	pathTracingUniforms.uResolution.value.y = context.drawingBufferHeight;
	pathTracingRenderTarget.setSize(context.drawingBufferWidth, context.drawingBufferHeight);
	screenCopyRenderTarget.setSize(context.drawingBufferWidth, context.drawingBufferHeight);
	worldCamera.aspect = SCREEN_WIDTH / SCREEN_HEIGHT;
	worldCamera.updateProjectionMatrix();

	// the following scales all scene objects by the worldCamera's field of view,
	// taking into account the screen aspect ratio and multiplying the uniform uULen,
	// the x-coordinate, by this ratio
	fovScale = worldCamera.fov * 0.5 * (Math.PI / 180.0);
	pathTracingUniforms.uVLen.value = Math.tan(fovScale);
	pathTracingUniforms.uULen.value = pathTracingUniforms.uVLen.value * worldCamera.aspect;

}

const directions = {
  ArrowUp: new THREE.Vector3(0, 0, -2),
  ArrowDown: new THREE.Vector3(0, 0, 2),
  ArrowLeft: new THREE.Vector3(-2, 0, 0),
  ArrowRight: new THREE.Vector3(2, 0, 0),
}

const avatar = {
  speed: new THREE.Vector3()
}

function keydown({ key }) {
  if(directions[key]) avatar.speed.add(directions[key]);
}

function init() {
	window.addEventListener('resize', onWindowResize, false);
  window.addEventListener('keydown', keydown);
	initTHREEjs(); // boilerplate: init necessary three.js items and scene/demo-specific objects
}

function initTHREEjs() {
	canvas = document.createElement('canvas');
	renderer = new THREE.WebGLRenderer({ canvas: canvas, context: canvas.getContext('webgl2') });
	//suggestion: set to false for production
	renderer.debug.checkShaderErrors = true;
	// renderer.autoClear = true; // TODO: try false
	renderer.toneMapping = THREE.ReinhardToneMapping;

	//required by WebGL 2.0 for rendering to FLOAT textures
	context = renderer.getContext();
	context.getExtension('EXT_color_buffer_float');

	container = document.getElementById('container');
	container.appendChild(renderer.domElement);

	clock = new THREE.Clock();

	pathTracingScene = new THREE.Scene();
	screenCopyScene = new THREE.Scene();
	screenOutputScene = new THREE.Scene();

	// quadCamera is simply the camera to help render the full screen quad (2 triangles),
	// hence the name.  It is an Orthographic camera that sits facing the view plane, which serves as
	// the window into our 3d world. This camera will not move or rotate for the duration of the app.
	quadCamera = new THREE.OrthographicCamera(-1, 1, 1, -1, 0, 1);
	screenCopyScene.add(quadCamera);
	screenOutputScene.add(quadCamera);

	// worldCamera is the dynamic camera 3d object that will be positioned, oriented and
	// constantly updated inside the 3d scene.  Its view will ultimately get passed back to the
	// stationary quadCamera, which renders the scene to a fullscreen quad (made up of 2 large triangles).
	worldCamera = new THREE.PerspectiveCamera(60, document.body.clientWidth / document.body.clientHeight, 1, 1000);
	pathTracingScene.add(worldCamera);

	controls = new THREE.OrbitControls(worldCamera, renderer.domElement);
	controls.target.set(300, 200, 100);

	// setup render targets...
	pathTracingRenderTarget = new THREE.WebGLRenderTarget(context.drawingBufferWidth, context.drawingBufferHeight, {
		minFilter: THREE.NearestFilter,
		magFilter: THREE.NearestFilter,
		format: THREE.RGBAFormat,
		type: THREE.FloatType,
		depthBuffer: false,
		stencilBuffer: false
	});
	pathTracingRenderTarget.texture.generateMipmaps = false;

	screenCopyRenderTarget = new THREE.WebGLRenderTarget(context.drawingBufferWidth, context.drawingBufferHeight, {
		minFilter: THREE.NearestFilter,
		magFilter: THREE.NearestFilter,
		format: THREE.RGBAFormat,
		type: THREE.FloatType,
		depthBuffer: false,
		stencilBuffer: false
	});
	screenCopyRenderTarget.texture.generateMipmaps = false;

	// blueNoise texture used in all demos
	blueNoiseTexture = new THREE.TextureLoader().load('textures/BlueNoise_RGBA256.png');
	blueNoiseTexture.wrapS = THREE.RepeatWrapping;
	blueNoiseTexture.wrapT = THREE.RepeatWrapping;
	blueNoiseTexture.flipY = false;
	blueNoiseTexture.minFilter = THREE.NearestFilter;
	blueNoiseTexture.magFilter = THREE.NearestFilter;
	blueNoiseTexture.generateMipmaps = false;


	// setup scene/demo-specific objects, variables, and data
	initSceneData();


	// setup screen-size quad geometry and shaders....

	// this full-screen quad mesh performs the path tracing operations and produces a screen-sized image
	pathTracingGeometry = new THREE.PlaneBufferGeometry(2, 2);
	pathTracingUniforms = {

		tPreviousTexture: { type: "t", value: screenCopyRenderTarget.texture },
		tBlueNoiseTexture: { type: "t", value: blueNoiseTexture },

		uCameraIsMoving: { type: "b1", value: false },
		uSceneIsDynamic: { type: "b1", value: sceneIsDynamic },

		uEPS_intersect: { type: "f", value: EPS_intersect },
		uTime: { type: "f", value: 0.0 },
		uSampleCounter: { type: "f", value: 0.0 },
		uFrameCounter: { type: "f", value: 1.0 },
		uULen: { type: "f", value: 1.0 },
		uVLen: { type: "f", value: 1.0 },
		uApertureSize: { type: "f", value: apertureSize },
		uFocusDistance: { type: "f", value: focusDistance },
		uSunAngularDiameterCos: { type: "f", value: sunAngularDiameterCos },

		uResolution: { type: "v2", value: new THREE.Vector2() },
		uRandomVec2: { type: "v2", value: new THREE.Vector2() },

		uCameraMatrix: { type: "m4", value: new THREE.Matrix4() }
	};

	initPathTracingShaders();


	// this full-screen quad mesh copies the image output of the pathtracing shader and feeds it back in to that shader as a 'previousTexture'
	screenCopyGeometry = new THREE.PlaneBufferGeometry(2, 2);

	screenCopyUniforms = {
		tPathTracedImageTexture: { type: "t", value: null }
	};

	fileLoader.load('shaders/ScreenCopy_Fragment.glsl', function (shaderText) {

		screenCopyFragmentShader = shaderText;

		screenCopyMaterial = new THREE.ShaderMaterial({
			uniforms: screenCopyUniforms,
			vertexShader: pathTracingVertexShader,
			fragmentShader: screenCopyFragmentShader,
			depthWrite: false,
			depthTest: false
		});

		screenCopyMaterial.uniforms.tPathTracedImageTexture.value = pathTracingRenderTarget.texture;

		screenCopyMesh = new THREE.Mesh(screenCopyGeometry, screenCopyMaterial);
		screenCopyScene.add(screenCopyMesh);
	});


	// this full-screen quad mesh takes the image output of the path tracing shader (which is a continuous blend of the previous frame and current frame),
	// and applies gamma correction (which brightens the entire image), and then displays the final accumulated rendering to the screen
	screenOutputGeometry = new THREE.PlaneBufferGeometry(2, 2);

	screenOutputUniforms = {
		uOneOverSampleCounter: { type: "f", value: 0.0 },
		tPathTracedImageTexture: { type: "t", value: null }
	};

	fileLoader.load('shaders/ScreenOutput_Fragment.glsl', function (shaderText) {

		screenOutputFragmentShader = shaderText;

		screenOutputMaterial = new THREE.ShaderMaterial({
			uniforms: screenOutputUniforms,
			vertexShader: pathTracingVertexShader,
			fragmentShader: screenOutputFragmentShader,
			depthWrite: false,
			depthTest: false
		});

		screenOutputMaterial.uniforms.tPathTracedImageTexture.value = pathTracingRenderTarget.texture;

		screenOutputMesh = new THREE.Mesh(screenOutputGeometry, screenOutputMaterial);
		screenOutputScene.add(screenOutputMesh);
	});

	// this 'jumpstarts' the initial dimensions and parameters for the window and renderer
	onWindowResize();
	animate();
}




function animate() {

	frameTime = clock.getDelta();

	elapsedTime = clock.getElapsedTime() % 1000;

  worldCamera.translateZ(avatar.speed.z);
  worldCamera.translateX(avatar.speed.x);
  const direction = new THREE.Vector3();
  worldCamera.getWorldDirection(direction);
  direction.multiplyScalar(100).add(worldCamera.position);
  controls.target.x = direction.x;
  controls.target.y = direction.y;
  controls.target.z = direction.z;

  if(Math.abs(avatar.speed.x) + Math.abs(avatar.speed.y) + Math.abs(avatar.speed.z) > .1)
    avatar.speed.multiplyScalar(.9);
  else avatar.speed.roundToZero();
	cameraPosition = worldCamera.position.clone();

	cameraIsMoving = !cameraPosition.equals(cameraPositionPrevious);

	// apertureSize = 500;

	if (windowIsBeingResized) {
		cameraIsMoving = true;
		windowIsBeingResized = false;
	}


	// update scene/demo-specific input(if custom), variables and uniforms every animation frame
	updateVariablesAndUniforms();

	// now update uniforms that are common to all scenes
	if (!cameraIsMoving)
	{
		if (sceneIsDynamic)
			sampleCounter = 1.0; // reset for continuous updating of image
		else sampleCounter += 1.0; // for progressive refinement of image

		frameCounter += 1.0;

		cameraRecentlyMoving = false;
	}

	if (cameraIsMoving) {
		sampleCounter = 1.0;
		frameCounter += 1.0;

		if (!cameraRecentlyMoving) {
			frameCounter = 1.0;
			cameraRecentlyMoving = true;
		}
	}


	pathTracingUniforms.uTime.value = elapsedTime;
	pathTracingUniforms.uCameraIsMoving.value = cameraIsMoving;
	pathTracingUniforms.uSampleCounter.value = sampleCounter;
	pathTracingUniforms.uFrameCounter.value = frameCounter;
	pathTracingUniforms.uRandomVec2.value.set(Math.random(), Math.random());

	// CAMERA
	// cameraControlsObject.updateMatrixWorld(true);
	worldCamera.updateMatrixWorld(true);
	pathTracingUniforms.uCameraMatrix.value.copy(worldCamera.matrixWorld);

	// PROGRESSIVE SAMPLE WEIGHT (reduces intensity of each successive animation frame's image)
	screenOutputUniforms.uOneOverSampleCounter.value = 1.0 / sampleCounter;

	// STEP 1
	// Perform PathTracing and Render(save) into pathTracingRenderTarget, a full-screen texture.
	// Read previous screenCopyRenderTarget(via texelFetch inside fragment shader) to use as a new starting point to blend with
	renderer.setRenderTarget(pathTracingRenderTarget);
	renderer.render(pathTracingScene, worldCamera);

	// STEP 2
	// Render(copy) the pathTracingScene output(pathTracingRenderTarget above) into screenCopyRenderTarget.
	// This will be used as a new starting point for Step 1 above (essentially creating ping-pong buffers)
	renderer.setRenderTarget(screenCopyRenderTarget);
	renderer.render(screenCopyScene, quadCamera);

	// STEP 3
	// Render full screen quad with generated pathTracingRenderTarget in STEP 1 above.
	// After applying tonemapping and gamma-correction to the image, it will be shown on the screen as the final accumulated output
	renderer.setRenderTarget(null);
	renderer.render(screenOutputScene, quadCamera);


	cameraPositionPrevious = cameraPosition.clone();
	requestAnimationFrame(animate);
}


// scene/demo-specific variables go here
var sceneIsDynamic = false;
// var camFlightSpeed = 300;
var tallBoxGeometry, tallBoxMaterial, tallBoxMesh;
var shortBoxGeometry, shortBoxMaterial, shortBoxMesh;

// called automatically from within initTHREEjs() function
function initSceneData() {

  // scene/demo-specific three.js objects setup goes here
  EPS_intersect = mouseControl ? 0.1 : 1.0; // less precision on mobile

  // Boxes
  tallBoxGeometry = new THREE.BoxGeometry(1,1,1);
  tallBoxMaterial = new THREE.MeshPhysicalMaterial( {
          color: new THREE.Color(0.95, 0.95, 0.95), //RGB, ranging from 0.0 - 1.0
          roughness: 1.0 // ideal Diffuse material
  } );

  tallBoxMesh = new THREE.Mesh(tallBoxGeometry, tallBoxMaterial);
  pathTracingScene.add(tallBoxMesh);
  tallBoxMesh.visible = false; // disable normal Three.js rendering updates of this object:
  // it is just a data placeholder as well as an Object3D that can be transformed/manipulated by
  // using familiar Three.js library commands. It is then fed into the GPU path tracing renderer
  // through its 'matrixWorld' matrix. See below:
  tallBoxMesh.rotation.set(0, Math.PI * 0.2, 0);
  tallBoxMesh.position.set(180, 170, -350);
  tallBoxMesh.updateMatrixWorld(true); // 'true' forces immediate matrix update


  shortBoxGeometry = new THREE.BoxGeometry(1,1,1);
  shortBoxMaterial = new THREE.MeshPhysicalMaterial( {
          color: new THREE.Color(0.95, 0.95, 0.95), //RGB, ranging from 0.0 - 1.0
          roughness: 1.0 // ideal Diffuse material
  } );

  shortBoxMesh = new THREE.Mesh(shortBoxGeometry, shortBoxMaterial);
  pathTracingScene.add(shortBoxMesh);
  shortBoxMesh.visible = false;
  shortBoxMesh.rotation.set(0, -Math.PI * 0.09, 0);
  shortBoxMesh.position.set(370, 85, -170);
  shortBoxMesh.updateMatrixWorld(true); // 'true' forces immediate matrix update


  // set camera's field of view
  worldCamera.fov = 75;
  worldCamera.position.set(300, 200, 200);

  focusDistance = 1000.0;
  window.camera = worldCamera;
}



// called automatically from within initTHREEjs() function
function initPathTracingShaders() {

  // scene/demo-specific uniforms go here
  pathTracingUniforms.uTallBoxInvMatrix = { type: "m4", value: new THREE.Matrix4() };
  pathTracingUniforms.uShortBoxInvMatrix = { type: "m4", value: new THREE.Matrix4() };

  pathTracingDefines = {
  	//NUMBER_OF_TRIANGLES: total_number_of_triangles
  };

  // load vertex and fragment shader files that are used in the pathTracing material, mesh and scene
  fileLoader.load('shaders/common_PathTracing_Vertex.glsl', function (shaderText) {
    pathTracingVertexShader = shaderText;

    createPathTracingMaterial();
  });
}


// called automatically from within initPathTracingShaders() function above
function createPathTracingMaterial() {

  fileLoader.load('shaders/Cornell_Box_Fragment.glsl', function (shaderText) {
    pathTracingFragmentShader = shaderText;
    pathTracingMaterial = new THREE.ShaderMaterial({
      uniforms: pathTracingUniforms,
      defines: pathTracingDefines,
      vertexShader: pathTracingVertexShader,
      fragmentShader: pathTracingFragmentShader,
      depthTest: false,
      depthWrite: false
    });

    pathTracingMesh = new THREE.Mesh(pathTracingGeometry, pathTracingMaterial);
    pathTracingScene.add(pathTracingMesh);

    // the following keeps the large scene ShaderMaterial quad right in front
    //   of the camera at all times. This is necessary because without it, the scene
    //   quad will fall out of view and get clipped when the camera rotates past 180 degrees.
    worldCamera.add(pathTracingMesh);
  });
}

function updateVariablesAndUniforms() {
  pathTracingUniforms.uTallBoxInvMatrix.value.copy(tallBoxMesh.matrixWorld).invert();
  pathTracingUniforms.uShortBoxInvMatrix.value.copy(shortBoxMesh.matrixWorld).invert();

}

init();
